﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreenFuseOrderSender
{
    public class GrowerOrderB2B
    {
        public Guid OrderGuid { get; set; }
        public string GrowerName { get; set; }
        public string CustomerPoNo { get; set; }
        public string OrderDescription { get; set; }
        public string OrderNo { get; set; }
        public string SellerOrderNo { get; set; }
        public string ProductFormCategoryCode { get; set; }
        public string ShipWeekString { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime OrderTime { get; set; }
        public DateTime ShipDate { get; set; }
        public string ShipMethodCode { get; set; }
        public string GrowerShipMethodCode { get; set; }
        public string ShipMethodDescription { get; set; }
        public string SubstitutionOK { get; set; }
        public string FreightPaymentMethod { get; set; }
        public string SellerNumber { get; set; }
        public string AirportCode { get; set; }
        public string BackOrderOK { get; set; }
        public string HeaderComments { get; set; }
        public string ShippingInstructions { get; set; }
        public string SalesRepName { get; set; }
        public string TagRatio { get; set; }
        public string TagRatioDescription { get; set; }
        public string GrowerID { get; set; }
        public string GrowerAddressName { get; set; }
        public string GrowerContact { get; set; }
        public string GrowerAddress1 { get; set; }
        public string GrowerAddress2 { get; set; }
        public string GrowerCity { get; set; }
        public string GrowerStateCode { get; set; }
        public string GrowerCountryCode { get; set; }
        public string GrowerZipCode { get; set; }
        public string GrowerFax { get; set; }
        public string GrowerPhone { get; set; }
        public string GrowerCountryCode2 { get; set; }
        public string GrowerAddressShipToID { get; set; }
        public string ShipToID { get; set; }
        public string ShipToName { get; set; }
        public string ShipToContact { get; set; }
        public string ShipToAddress1 { get; set; }
        public string ShipToAddress2 { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToStateCode { get; set; }
        public string ShipToCountryCode { get; set; }
        public string ShipToZipCode { get; set; }
        public string ShipToFax { get; set; }
        public string ShipToPhone { get; set; }
        public string ShipToCountryCode2 { get; set; }
        public string SupplierUrlForOrder { get; set; }
        public string GrowerURLForOrder { get; set; }
        public string B2BCustomerName { get; set; }
        public string GrowerEmail { get; set; }
        public string BrokerOrderNo { get; set; }
        public long RevisionNumber { get; set; }
    }
}
