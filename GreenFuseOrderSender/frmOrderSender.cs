﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Net.Http;
using WinSCP;







namespace GreenFuseOrderSender
{
    public partial class frmOrderSender : Form
    {
        public frmOrderSender()
        {
            InitializeComponent();
        }
        public string authToken;
        public string response;
        public string errorMessage;
        private void frmOrderSender_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer1.Interval = 5000;  //wait 5 seconds on first go
            
            LoadSendList();
        }

        private async void GetMcHutchVaugnsOrders()
        {
            try
            {
                //delete old files
                var thePath = @"c:\Temp\MchutchAvail\OrdersIn";
                System.IO.DirectoryInfo di = new DirectoryInfo(thePath);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }




                //transfer the file
                var sourcePath = "/prod/Outbox";
            //    var sourcePath = "/test/Outbox";
                var destinationFtpUrl = thePath;
                SessionOptions sessionOptions = new SessionOptions();
                //     sessionOptions.Protocol = Protocol.sftp;
                sessionOptions.Protocol = Protocol.Ftp;
                sessionOptions.HostName = "edi.mchutchison.com";
                sessionOptions.UserName = "GreenFuse";
                sessionOptions.Password = "aS481k4zNKL5yJpN";
                // sessionOptions.SshHostKeyFingerprint = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                Session session = new Session();
                session.Open(sessionOptions);
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;
                TransferOperationResult transferResult;
                RemovalOperationResult removalOperationResult;


                //This is for Getting/Downloading files from SFTP  
                transferResult = session.GetFiles(sourcePath, destinationFtpUrl, false, transferOptions);
                transferResult.Check();



                //now remove the files on the outbox
                removalOperationResult = session.RemoveFiles(sourcePath);

                //close session
                session.Close();



                string xml = "";
                var d = new DirectoryInfo(thePath);
                foreach (FileInfo fi in d.GetFiles())
                {
                    xml = "";
                    string uri = "";
                    string EPSusername = "";
                    string EPSpassword = "";
                    //Get the XML
                    xml = System.IO.File.ReadAllText(fi.FullName);

                    if (fi.FullName.IndexOf("MCH") > 0)
                    {
                        uri = "https://admin.green-fuse.com/ExternalGrower/OrderB2B?PublicKey=C6524716-84B4-43BC-BB86-242AA070A0ED";

                        //set authentication
                        EPSusername = "confirmations@mchutchison.com";
                        EPSpassword = "A99!GudsR";
                    }
                    else //vaughns
                    {
                        uri = "https://admin.green-fuse.com/ExternalGrower/OrderB2B?PublicKey=722F77C0-D35A-43DE-81D6-521FBBA0DB36";

                        //set authentication
                        EPSusername = "confirmations@vaughans.com";
                        EPSpassword = "zY!9IODRGS";
                    }
                    //Now send the files to API
                   
                    String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(EPSusername + ":" + EPSpassword));


                    //Set up request
                    var client = new HttpClient();
                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri(uri),
                        Method = HttpMethod.Post,
                    };
                    request.Headers.Add("Authorization", "Basic " + encoded);

                    //Set up Content
                    byte[] bytes = Encoding.UTF8.GetBytes(xml);
                    request.Content = new ByteArrayContent(bytes);

                    //Send the Request
                    var orderResponse = await client.SendAsync(request);

                    //Do what you want with the Response
                   // MessageBox.Show("Response from admin site: " + orderResponse.StatusCode + " " + orderResponse.ReasonPhrase);

                }
               
            }
            catch (Exception ex)
            {
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in GetMcHutchOrders: " + ex.Message, "From Broker", "", "");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }
        }
        private void LoadSendList()
        {
            try
            {


                DBStuff oDBStuff = new DBStuff();
                oDBStuff.sEnvironment = "Prod";
                oDBStuff.sConnectionType = "Data";

                //Auth Users
                string sSql = "EXEC B2BGetConfirmationsToSend";

                DataTable theTable = oDBStuff.GetDataFromSQL(sSql);
                dgSending.Rows.Clear();
                for (int i = 0; i < theTable.Rows.Count; i++)
                {

                    dgSending.Rows.Add("", "", "", "", "", "", "","","","");
                    dgSending.Rows[i].Cells[0].Value = theTable.Rows[i]["B2BStatus"];
                    dgSending.Rows[i].Cells[1].Value = theTable.Rows[i]["StatusMessage"];
                    dgSending.Rows[i].Cells[2].Value = theTable.Rows[i]["GrowerName"];
                    dgSending.Rows[i].Cells[3].Value = theTable.Rows[i]["GrowerEmail"];
                    dgSending.Rows[i].Cells[4].Value = theTable.Rows[i]["GrowerB2BOrderURL"];
                    dgSending.Rows[i].Cells[5].Value = theTable.Rows[i]["OrderGuid"];
                    dgSending.Rows[i].Cells[6].Value = theTable.Rows[i]["GrowerGuid"];
                    dgSending.Rows[i].Cells[7].Value = theTable.Rows[i]["BrokerName"];
                    dgSending.Rows[i].Cells[8].Value = theTable.Rows[i]["BrokerGuid"];
                    dgSending.Rows[i].Cells[9].Value = theTable.Rows[i]["BrokerB2BOrderURL"];
                    dgSending.Rows[i].Cells[10].Value = theTable.Rows[i]["B2BUserName"];
                    dgSending.Rows[i].Cells[11].Value = theTable.Rows[i]["B2BPassword"];



                    dgSending.Rows[i].Cells[0].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[1].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[2].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[3].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[4].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[5].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[6].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[7].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[8].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[9].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[10].Style.BackColor = Color.Wheat;
                    dgSending.Rows[i].Cells[11].Style.BackColor = Color.Wheat;


                }
                dgSending.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dgSending.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            }
            catch (Exception ex)
            {
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in LoadSendList: " + ex.Message, "To Grower","","");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }
        
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            try {
                label1.Text = "starting ticker";

                if(ckSendBrokerOrders.Checked == true)
                {
                    SendOrders();
                }

                if (ckAvailToMcHutch.Checked == true)
                {

                    //only send once every 4 hours
                    var checknightly = Convert.ToInt32(DateTime.Now.ToString("HHmmss"));
                    if (checknightly > 75000 && checknightly < 75500)
                    {
                        SendAvailToMcHutch();
                    }
                    if (checknightly > 115000 && checknightly < 115500)
                    {
                        SendAvailToMcHutch();
                    }
                    if (checknightly > 155000 && checknightly < 155500)
                    {
                        SendAvailToMcHutch();
                    }
                    if (checknightly > 195000 && checknightly < 195500)
                    {
                        SendAvailToMcHutch();
                    }
                    if (checknightly > 235000 && checknightly < 235500)
                    {
                        SendAvailToMcHutch();
                    }


                }
            }




            catch (Exception ex)
            {
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in timer1_Tick: " + ex.Message, "To Grower", "", "");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }
            finally
            {
                timer1.Interval = 4000 * 60;  //wait 4 minutes after first go
            }
        }
        private void SendAvailToMcHutch()
        {
            try
            {

                //get the data
                var publicKey = "9CB1183C-20AC-4049-9206-25C558CA5BEF";
                var shipWeekStart = "202130"; //To do
                var shipWeekEnd = "202229";//to do
                var growerGuid = new Guid(publicKey);
                var shipweekStart = Convert.ToInt32(shipWeekStart);
                var shipweekEnd = Convert.ToInt32(shipWeekEnd);
                var programCodes = "STPURCA,STPURCPER,INNURCA,INNURCPER,VMXURCA,VMXURCPER";
               
                var availabilityXML = "";
                var brokerGuid = new Guid("C6524716-84B4-43BC-BB86-242AA070A0ED");

                var service = new GrowerOrderDataService();
                var bret = service.GrowerAvailabilityGetForBroker(growerGuid, programCodes, shipweekStart, shipweekEnd, false, brokerGuid, out availabilityXML);


                //delete old files
                var thePath = @"c:\Temp\MchutchAvail";
                System.IO.DirectoryInfo di = new DirectoryInfo(thePath);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

                //write the file
                var fileExt= DateTime.Now.ToString("yyyyMMddHHmmss");
                var filename = thePath + @"\GreenFuse_Avail_" + fileExt + ".xml";
                File.WriteAllText(filename, availabilityXML);

               
               //transfer the file
                var sourcePath = @"c:\Temp\MchutchAvail";
                var destinationFtpUrl = "/prod/Avail";
                SessionOptions sessionOptions = new SessionOptions();
           //     sessionOptions.Protocol = Protocol.sftp;
                sessionOptions.Protocol = Protocol.Ftp;
                sessionOptions.HostName = "edi.mchutchison.com";
                sessionOptions.UserName = "GreenFuse";
                sessionOptions.Password = "aS481k4zNKL5yJpN";
               // sessionOptions.SshHostKeyFingerprint = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                Session session = new Session();
                session.Open(sessionOptions);
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;
                TransferOperationResult transferResult;
               
                
                
                //This is for Getting/Downloading files from SFTP  
                //transferResult = session.GetFiles(sourcePath, destinationFtpUrl, false, transferOptions);
                
                
                
                //This is for Putting/Uploading file on SFTP  
                transferResult = session.PutFiles(sourcePath, destinationFtpUrl, false, transferOptions);
                transferResult.Check();

                session.Close();
            }
            
            catch (Exception ex)
            {
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Avail Sender Error", "Error in SendAvailToMcHutch: " + ex.Message, "To Broker", "", "");

                if (bRet == false)
                {
                    MessageBox.Show("Email failed to send");
                }
            }
            finally
            {
               
            }
        }

        private void SendOrders()
        {
            try
            {


                var bRet = false;

                label1.Text = "starting SendOrders";

                for (int i = 0; i < dgSending.Rows.Count; i++)
                {


                    string growerName = (string)dgSending.Rows[i].Cells[2].Value;
                    Guid growerGuid = new Guid(dgSending.Rows[i].Cells[6].Value.ToString());
                    string brokerName = (string)dgSending.Rows[i].Cells[7].Value;
                    Guid brokerGuid = new Guid(dgSending.Rows[i].Cells[8].Value.ToString());
                    Guid orderGuid = new Guid(dgSending.Rows[i].Cells[5].Value.ToString());
                    string growerUrl = (string)dgSending.Rows[i].Cells[4].Value;
                    string brokerUrl = (string)dgSending.Rows[i].Cells[9].Value;
                    string username = (string)dgSending.Rows[i].Cells[10].Value;
                    string password = (string)dgSending.Rows[i].Cells[11].Value;
                    errorMessage = "";
                    bRet = SendOrder(growerName, growerGuid, brokerName,brokerGuid, orderGuid, growerUrl, brokerUrl, username, password);
                    if (bRet == false)
                    {
                        throw new Exception("error: " + errorMessage + " orderGuid: " + orderGuid);
                           
                    }

                }
                label1.Text = "reloading";
                LoadSendList();
                label1.Text = "done reloading";
            }
            catch (Exception ex)
            {
              
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in SendOrders: " + ex.Message, "To Grower", "", "");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }
    
        }
        private bool SendOrder(string growerName,Guid growerGuid, string brokerName,Guid brokerGuid,Guid orderGuid, string growerUrl, string brokerUrl, string B2BUserName, string B2BPassword)
        {
            

            
            var bRet = true;
          
            try
            {
               
               
                var XMLstring = "";
               // var localResponse = "";
                var oService = new GrowerOrderDataService();


             
                
                
                var growerOrder = oService.GetB2BOrder(orderGuid, out errorMessage);
                if (errorMessage != "")
                {
                    return false;
                }

                var orderLines = oService.GetOrderLines(orderGuid, out errorMessage);
                if (errorMessage != "")
                {
                    return false;
                }

                XMLstring = GenerateXMLString(brokerName,growerOrder, orderLines,1);  //1 = Color Point, Picas does not use this

                switch (brokerName)
                {
                    case "Ball Horticultural":
                   
                        {
                            SendB2BOrderToBall(XMLstring, growerOrder.GrowerName, brokerName , brokerUrl, B2BUserName,B2BPassword);
                            break;
                        }
                    case "McHutchison":

                        {
                            SendB2BOrderToMcHutchison(XMLstring, growerOrder.GrowerName, brokerName, brokerUrl, B2BUserName, B2BPassword, growerOrder.OrderNo);
                            break;
                        }
                    case "Vaughans":

                        {
                            SendB2BOrderToVaughans(XMLstring, growerOrder.GrowerName, brokerName, brokerUrl, B2BUserName, B2BPassword, growerOrder.OrderNo);
                            break;
                        }
                    default:
                        { 
                            break;
                        }
                }


                

                


                var jobNumber = oService.B2BIntegrationAddXMLData("Acknowledgement", "ToGrower", "GFB", "GFB", XMLstring, "<Response></Response>", orderGuid, "Sent", "URL:" + growerOrder.GrowerURLForOrder, growerGuid, brokerGuid,out errorMessage);
                if (errorMessage != "")
                {
                    bRet = false;
                }

                bRet = oService.B2BAfterGrowerSendUpdate(orderGuid, out errorMessage);
                if (errorMessage != "")
                {
                    return false;
                }



            }

            catch (Exception ex)
            {
                var oDB = new DBStuff();
                bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in SendOrder: " + ex.Message, "To Broker", brokerName, orderGuid.ToString());

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
                bRet = false;
            }
         
            finally
            {

            }

            return bRet;

        }


        private async void SendB2BOrderToExpress(string XMLstring, string growerName, string customer, string url, string B2BUserName, string B2BPassword)
        {
            var responseXMLString = "";
            string username = "";
            string password = "";

            try
            {



                password = B2BPassword;
                username = "GreenFuse";
                password = "GVtwtV";


                DBStuff dbStuff = new DBStuff();
                dbStuff.B2BLogMessage("Sending Order B2B to " + customer + ".");

                var orderUri = url;
                String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                var client = new HttpClient();
                var requestExpress = new HttpRequestMessage()
                {
                    RequestUri = new Uri(orderUri),
                    Method = HttpMethod.Post,
                };
                requestExpress.Headers.Add("Authorization", "Basic " + encoded);

                //Set up Content
                //  byte[] bytes = Encoding.UTF8.GetBytes(XMLstring);
                //  requestBall.Content = new ByteArrayContent(bytes);

                requestExpress.Content = new StringContent(XMLstring,
                                     Encoding.UTF8,
                                     "Text/XML");//CONTENT-TYPE header

                // requestBall.PreAuthenticate = true;
                //Send the Request
                var availResponse = await client.SendAsync(requestExpress);


                //Log the response
                var expressResponse = "StatusCode: " + availResponse.StatusCode + ", ReasonPhrase: " + availResponse.ReasonPhrase;
                response = "<Response>" + expressResponse + "</Response>";
                dbStuff.B2BLogMessage("Finished Sending Order B2B to " + customer + ". Response was: " + expressResponse);


            }
            catch (Exception ex)
            {
                responseXMLString = "<Error>" + ex.Message + "</Error>";
                response = responseXMLString;
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in SendB2BOrderToExpress: " + ex.Message, "To Broker", customer, "");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }

            return;
        }
        private async void SendB2BOrderToBall(string XMLstring,string growerName, string customer, string url, string B2BUserName, string B2BPassword)
        {
            var responseXMLString = "";
            string username = "";
            string password = "";

            try
            {


                XMLstring = XMLstring.Replace("tm", "");
                username = B2BUserName;
                password = B2BPassword;
                username = "GRFUREMOTE";
                password = "bhc45gr$";


               DBStuff dbStuff = new DBStuff();
                dbStuff.B2BLogMessage("Sending Order B2B to " + customer + ".");
               
                var orderUri = url;
                String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                var client = new HttpClient();
                var requestBall = new HttpRequestMessage()
                {
                    RequestUri = new Uri(orderUri),
                    Method = HttpMethod.Post,
                };
                requestBall.Headers.Add("Authorization", "Basic " + encoded);

                //Set up Content
              //  byte[] bytes = Encoding.UTF8.GetBytes(XMLstring);
              //  requestBall.Content = new ByteArrayContent(bytes);

                requestBall.Content = new StringContent(XMLstring,
                                     Encoding.UTF8,
                                     "Text/XML");//CONTENT-TYPE header

                // requestBall.PreAuthenticate = true;
                //Send the Request
                var availResponse = await client.SendAsync(requestBall);


                //Log the response
                var ballResponse = "StatusCode: " + availResponse.StatusCode + ", ReasonPhrase: " + availResponse.ReasonPhrase ;
                response = "<Response>" + ballResponse + "</Response>";
                dbStuff.B2BLogMessage("Finished Sending Order B2B to " + customer + ". Response was: " + ballResponse);

               
            }
            catch (Exception ex)
            {
                responseXMLString = "<Error>" + ex.Message + "</Error>";
                response = responseXMLString;
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in SendB2BOrderToBall: " + ex.Message, "To Broker", customer, "");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }

            return;
        }

        private async void SendB2BOrderToVaughans(string XMLstring, string growerName, string customer, string url, string B2BUserName, string B2BPassword, string OrderNo)
        {
            var responseXMLString = "";
            string username = "";
            string password = "";

            try
            {

                var thePath = @"c:\Temp\Vaughans\OrdersOut";


                //write the file
                var fileExt = DateTime.Now.ToString("yyyyMMddHHmmss");
                var filename = thePath + @"\GreenFuse_" + OrderNo + "_" + fileExt + ".xml";
                File.WriteAllText(filename, XMLstring);


                //transfer the file
                var sourcePath = thePath;
                var destinationFtpUrl = "/prod/VAU/ACK/Inbox";
               // var destinationFtpUrl = "/test/VAU/ACK/Inbox";
                SessionOptions sessionOptions = new SessionOptions();
                //     sessionOptions.Protocol = Protocol.sftp;
                sessionOptions.Protocol = Protocol.Ftp;
                sessionOptions.HostName = "edi.mchutchison.com";
                sessionOptions.UserName = "GreenFuse";
                sessionOptions.Password = "aS481k4zNKL5yJpN";
                // sessionOptions.SshHostKeyFingerprint = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                Session session = new Session();
                session.Open(sessionOptions);
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;
                TransferOperationResult transferResult;



                //This is for Getting/Downloading files from SFTP  
                //transferResult = session.GetFiles(sourcePath, destinationFtpUrl, false, transferOptions);



                //This is for Putting/Uploading file on SFTP  
                transferResult = session.PutFiles(sourcePath, destinationFtpUrl, false, transferOptions);
                transferResult.Check();

                session.Close();

                //delete old files
                System.IO.DirectoryInfo di = new DirectoryInfo(thePath);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

            }
            catch (Exception ex)
            {
                responseXMLString = "<Error>" + ex.Message + "</Error>";
                response = responseXMLString;
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in SendB2BOrderToMcHutchison: " + ex.Message, "To Broker", customer, "");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }

            return;
        }

        private async void SendB2BOrderToMcHutchison(string XMLstring, string growerName, string customer, string url, string B2BUserName, string B2BPassword,string OrderNo)
        {
            var responseXMLString = "";
            string username = "";
            string password = "";

            try
            {


              


             
                var thePath = @"c:\Temp\MchutchAvail\OrdersOut";
             

                //write the file
                var fileExt = DateTime.Now.ToString("yyyyMMddHHmmss");
                var filename = thePath + @"\GreenFuse_" + OrderNo + "_" + fileExt + ".xml";
                File.WriteAllText(filename, XMLstring);


                //transfer the file
                var sourcePath = thePath;
          //      var destinationFtpUrl = "/test/MCH/ACK/Inbox";
                var destinationFtpUrl = "/prod/MCH/ACK/Inbox";
                SessionOptions sessionOptions = new SessionOptions();
                //     sessionOptions.Protocol = Protocol.sftp;
                sessionOptions.Protocol = Protocol.Ftp;
                sessionOptions.HostName = "edi.mchutchison.com";
                sessionOptions.UserName = "GreenFuse";
                sessionOptions.Password = "aS481k4zNKL5yJpN";
                // sessionOptions.SshHostKeyFingerprint = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                Session session = new Session();
                session.Open(sessionOptions);
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;
                TransferOperationResult transferResult;



                //This is for Getting/Downloading files from SFTP  
                //transferResult = session.GetFiles(sourcePath, destinationFtpUrl, false, transferOptions);

               
                //This is for Putting/Uploading file on SFTP  
                transferResult = session.PutFiles(sourcePath, destinationFtpUrl, false, transferOptions);
                transferResult.Check();

                session.Close();

                //delete old files
                System.IO.DirectoryInfo di = new DirectoryInfo(thePath);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

            }
            catch (Exception ex)
            {
                responseXMLString = "<Error>" + ex.Message + "</Error>";
                response = responseXMLString;
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in SendB2BOrderToMcHutchison: " + ex.Message, "To Broker", customer, "");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }

            return;
        }


        private void cmdSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (ckSendBrokerOrders.Checked == true)
                {
                    SendOrders();
                }
                if (ckAvailToMcHutch.Checked == true)
                {
                    SendAvailToMcHutch();
                }
                if (ckGetMchutch.Checked == true)
                {
                    GetMcHutchVaugnsOrders();
                }

            }
            catch(Exception ex)
            {
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in cmdSend_Click: " + ex.Message, "To Grower", "", "");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }
        }
        private string GenerateXMLString(string brokerName, GrowerOrderB2B growerOrder, List<OrderLine> orderLines,long clientID)
        {
            string XMLstring = "";
            try
            {


               
                string productDescription = "";
                string lineComment = "";
                switch (brokerName)
                {
                    case "Ball Horticultural":
                  
                        {
                            var growerShipMethod = "";
                            var plantCode = "";
                            var plantCodeDescription = "";
                            var shippingPoint = "";
                            var shippingPointDescription= "";
                            if (growerOrder.GrowerShipMethodCode == "")
                            {
                                growerShipMethod = "10";
                            }
                            else
                            {
                                growerShipMethod = growerOrder.GrowerShipMethodCode;
                            }
                            var currentDate = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");//2016-03-21T12:11:05

                            XMLstring = "<BrokerAcknowledgement> ";
                            XMLstring += "<Identification Reference_ID='" + growerOrder.OrderGuid + "' Date='" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss") + "' MessageProtocol_ID='BrokerAcknowledgement' FromOrg_ID='GREENFUSE BOTANICALS' ToOrg_ID='Ball Seed' SenderOrg_ID=''";
                            XMLstring += " />";

                            XMLstring += "<OrderHeader";
                            XMLstring += " ChangeOrderBatchNumber='" + growerOrder.RevisionNumber + "'";
                            XMLstring += " VendorOrderID='" + growerOrder.OrderNo + "'";
                            XMLstring += " BrokerOrderID='" + growerOrder.BrokerOrderNo + "'";
                            XMLstring += " CustomerPONumber='" + growerOrder.CustomerPoNo + "'";
                            XMLstring += " BrokerCreatedDate='" + growerOrder.OrderTime.ToString("yyyy-MM-dd") + "'";
                            XMLstring += " BrokerCreatedTime='" + growerOrder.OrderTime.ToString("Thh:mm:ss") + "'";
                            XMLstring += " RequestedShipDate='" + growerOrder.ShipDate.ToString("yyyy-MM-dd") + "'";
                            XMLstring += " ShipMethod='" + "06" + "'";
                            XMLstring += " ShipMethodDesc='" + "FedEx Overnight" + "'";
                            XMLstring += " SubstitutionOK='" + "1" + "'";
                            XMLstring += " FreightPaymentMethod='" + "PPA" + "'";
                            XMLstring += " VendorNumber='" + "0000127009" + "'";
                            XMLstring += " AirportCode='" + "ORD" + "'";
                            XMLstring += " BackOrderOK='" + "0" + "'";
                            XMLstring += " ShippingInstructions='" + "DO NOT SHIP - TEST ORDER" + "'";
                            XMLstring += " HeaderComments='" + "" + "'";
                            XMLstring += " IncomingType='" + "N" + "'";
                            XMLstring += " SalesRepName='" + "" + "'";
                            XMLstring += " TagRatio='" + "M0" + "'";
                            XMLstring += " TagRatioDescription='" + "Tag N/A from Ball" + "'";
                            XMLstring += " >";

                            XMLstring += "<SoldTo";
                            XMLstring += " BrokerSoldToNumber='" + "" + "'";
                            XMLstring += " SoldToName='" + growerOrder.GrowerName + "'";
                            XMLstring += " SoldToContactName='" + "" + "'";
                            XMLstring += " SoldToAddress1='" + growerOrder.ShipToAddress1 + "'";
                            XMLstring += " SoldToCity='" + growerOrder.ShipToCity + "'";
                            XMLstring += " SoldToState='" + growerOrder.ShipToStateCode + "'";
                            XMLstring += " SoldToCountry='" + growerOrder.ShipToCountryCode + "'";
                            XMLstring += " SoldToZip='" + growerOrder.ShipToZipCode + "'";
                            XMLstring += " SoldToFax='" + "" + "'";
                            XMLstring += " SoldToPhone='" + growerOrder.ShipToPhone + "'";
                            XMLstring += " SoldToCountry3='" + "" + "'";
                            XMLstring += " />";

                            XMLstring += "<ShipTo";
                            XMLstring += " BrokerShipToNumber='" + "" + "'";
                            XMLstring += " ShipToName='" + growerOrder.GrowerName + "'";
                            XMLstring += " ShipToContactName='" + "" + "'";
                            XMLstring += " ShipToAddress1='" + growerOrder.ShipToAddress1 + "'";
                            XMLstring += " ShipToCity='" + growerOrder.ShipToCity + "'";
                            XMLstring += " ShipToState='" + growerOrder.ShipToStateCode + "'";
                            XMLstring += " ShipToCountry='" + growerOrder.ShipToCountryCode + "'";
                            XMLstring += " ShipToZip='" + growerOrder.ShipToZipCode + "'";
                            XMLstring += " ShipToFax='" + "" + "'";
                            XMLstring += " ShipToPhone='" + growerOrder.ShipToPhone + "'";
                            XMLstring += " ShipToCountry3='" + "" + "'";
                            XMLstring += " ShipToNonStandardAddress='" + "0" + "'";
                            XMLstring += " />";

                            XMLstring += "</OrderHeader>";

                            XMLstring += "<OrderDetails>";
                            foreach (var item in orderLines)
                            {
                          
                                switch (item.SupplierCode)
                                {
                                    case "VMX":

                                        {
                                            plantCode = "GRFV";
                                            plantCodeDescription = "Greenfuse Botanicals - Vivero";
                                            shippingPoint = "GRFV";
                                            shippingPointDescription = "Greenfuse Botanicals - Vivero";
                                            break;
                                        }
                                    case "INN":

                                        {
                                            plantCode = "GRFI";
                                            plantCodeDescription = "Greenfuse Botanicals - Innova";
                                            shippingPoint = "GRFI";
                                            shippingPointDescription = "Greenfuse Botanicals - Innova";
                                            break;
                                        }
                                    case "STP":

                                        {
                                            plantCode = "GRFU";
                                            plantCodeDescription = "Greenfuse Botanicals - Santa Paula";
                                            shippingPoint = "GRFU";
                                            shippingPointDescription = "Greenfuse Botanicals - Santa Paula";
                                            break;
                                        }
                                    default:
                                        {
                                            plantCode = "UNK";
                                            plantCodeDescription = "UNK";
                                            shippingPoint = "UNK";
                                            shippingPointDescription = "UNK";

                                            break;
                                        }
                                }






                             //   productDescription = "HI";
                                productDescription = item.SupplierProductDescription.Replace("'", "") + " " + item.ProductFormCategory;
                                productDescription.Replace("&", " ");
                                lineComment = item.LineComment.Replace("'", "");
                                lineComment = item.LineComment.Replace("&", " and ");

                                XMLstring += "<Item ";
                                XMLstring += " BrokerLineItemNumber='" + item.ID + "'";
                                XMLstring += " BrokerMaterialNumber='" + "" + "'";
                                XMLstring += " VendorMaterialNumber='" + item.ProductID + "'";
                                XMLstring += " MaterialDescription='" + productDescription + "'";
                                XMLstring += " OrderdQty='" + item.OrderQty + "'";
                                XMLstring += " ConfirmedQty='" + item.OrderQty + "'";
                                XMLstring += " UnitOfMeasure='" + "EA" + "'";
                                XMLstring += " ScheduledShipDate='" + growerOrder.ShipDate.ToString("yyyy-MM-dd") + "'";
                                if (item.OrderQty == 0)
                                {
                                    XMLstring += " LineItemStatus='" + "C" + "'";
                                }
                                else
                                {
                                    if(item.OrderLineStatus== "Supplier Add")
                                    {
                                        XMLstring += " LineItemStatus='" + "N" + "'";

                                    }
                                    else
                                    {
                                        XMLstring += " LineItemStatus='" + "M" + "'";

                                    }
                                }
                                


                                XMLstring += " TagRatio='" + "M0" + "'";
                                XMLstring += " TagRatioDescription='" + "Tag N/A from Ball" + "'";
                                XMLstring += " PlantCode='" + plantCode + "'";
                                XMLstring += " PlantCodeDescription='" + plantCodeDescription + "'";
                                XMLstring += " ShippingPoint='" + shippingPoint + "'";
                                XMLstring += " ShippingPointDescription='" + shippingPointDescription + "'";
                                XMLstring += " MaterialGroup='" + "" + "'";
                                XMLstring += " />";
                          

                            }
                            XMLstring += "</OrderDetails>";

                            XMLstring += "</BrokerAcknowledgement> ";

                            break;
                        }

                    case "McHutchison":
                        {
                            var tagRatio = growerOrder.TagRatio;
                            if (tagRatio == "NoTag")
                            {
                                tagRatio = "0";
                            }
                            if (tagRatio == "TagInc")
                            {
                                tagRatio = "1";
                            }
                            var growerShipMethod = "";
                            var plantCode = "";
                            var plantCodeDescription = "";
                            var shippingPoint = "";
                            var shippingPointDescription = "";
                            if (growerOrder.GrowerShipMethodCode == "")
                            {
                                growerShipMethod = "FEDEX";
                            }
                            else
                            {
                                growerShipMethod = growerOrder.GrowerShipMethodCode;
                            }
                            var currentDate = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");//2016-03-21T12:11:05

                            XMLstring = "<McHutchisonPurchaseOrder> ";
                            XMLstring += "<Identification Reference_ID='" + growerOrder.OrderGuid + "' Date='" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss") + "' MessageProtocol_ID='McHutchisonPurchaseOrder' FromOrg_ID='GFM' ToOrg_ID='NAV' SenderOrg_ID='NAV'";
                            XMLstring += " />";

                            XMLstring += "<OrderHeader";
                            XMLstring += " VendorOrderID='" + growerOrder.OrderNo + "'";
                            XMLstring += " BrokerOrderID='" + growerOrder.BrokerOrderNo + "'";
                            XMLstring += " CustomerPONumber='" + growerOrder.CustomerPoNo + "'";
                            XMLstring += " BrokerCreatedDate='" + growerOrder.OrderTime.ToString("yyyy-MM-dd") + "'";
                            XMLstring += " BrokerCreatedTime='" + growerOrder.OrderTime.ToString("Thh:mm:ss") + "'";
                            XMLstring += " RequestedShipDate='" + growerOrder.ShipDate.ToString("yyyy-MM-dd") + "'";
                            XMLstring += " ShipMethod='" + growerShipMethod + "'";
                            XMLstring += " ShipMethodDesc='" + "" + "'";
                            XMLstring += " SubstitutionOK='" + "1" + "'";
                            XMLstring += " VendorNumber='" + "" + "'";
                            XMLstring += " HeaderComments='" + growerOrder.OrderDescription + "'";
                            XMLstring += " IncomingType='" + "N" + "'";
                            XMLstring += " SalesRepName='" + "" + "'";
                            XMLstring += " TagRatio='" + tagRatio + "'";
                            XMLstring += " TagRatioDescription='" + "" + "'";
                            XMLstring += " >";

                            XMLstring += "<SoldTo";
                            XMLstring += " BrokerSoldToNumber='" + "" + "'";
                            XMLstring += " SoldToName='" + growerOrder.GrowerName + "'";
                            XMLstring += " SoldToContactName='" + "" + "'";
                            XMLstring += " SoldToAddress1='" + growerOrder.ShipToAddress1 + "'";
                            XMLstring += " SoldToCity='" + growerOrder.ShipToCity + "'";
                            XMLstring += " SoldToState='" + growerOrder.ShipToStateCode + "'";
                            XMLstring += " SoldToCountry='" + growerOrder.ShipToCountryCode + "'";
                            XMLstring += " SoldToZip='" + growerOrder.ShipToZipCode + "'";
                            XMLstring += " SoldToFax='" + "" + "'";
                            XMLstring += " SoldToPhone='" + growerOrder.ShipToPhone + "'";
                            XMLstring += " />";

                            XMLstring += "<ShipTo";
                            XMLstring += " BrokerShipToNumber='" + "" + "'";
                            XMLstring += " ShipToName='" + growerOrder.GrowerName + "'";
                            XMLstring += " ShipToContactName='" + "" + "'";
                            XMLstring += " ShipToAddress1='" + growerOrder.ShipToAddress1 + "'";
                            XMLstring += " ShipToCity='" + growerOrder.ShipToCity + "'";
                            XMLstring += " ShipToState='" + growerOrder.ShipToStateCode + "'";
                            XMLstring += " ShipToCountry='" + growerOrder.ShipToCountryCode + "'";
                            XMLstring += " ShipToZip='" + growerOrder.ShipToZipCode + "'";
                            XMLstring += " ShipToFax='" + "" + "'";
                            XMLstring += " ShipToPhone='" + growerOrder.ShipToPhone + "'";
                            XMLstring += " />";

                            XMLstring += "</OrderHeader>";

                            XMLstring += "<OrderDetails>";
                            foreach (var item in orderLines)
                            {

                                switch (item.SupplierCode)
                                {
                                    case "VMX":

                                        {
                                            plantCode = "GRFV";
                                            plantCodeDescription = "Greenfuse Botanicals - Vivero";
                                            shippingPoint = "GRFV";
                                            shippingPointDescription = "Greenfuse Botanicals - Vivero";
                                            break;
                                        }
                                    case "INN":

                                        {
                                            plantCode = "GRFI";
                                            plantCodeDescription = "Greenfuse Botanicals - Innova";
                                            shippingPoint = "GRFI";
                                            shippingPointDescription = "Greenfuse Botanicals - Innova";
                                            break;
                                        }
                                    case "STP":

                                        {
                                            plantCode = "GRFS";
                                            plantCodeDescription = "Greenfuse Botanicals - Santa Paula";
                                            shippingPoint = "GRFS";
                                            shippingPointDescription = "Greenfuse Botanicals - Santa Paula";
                                            break;
                                        }
                                    default:
                                        {
                                            plantCode = "UNK";
                                            plantCodeDescription = "UNK";
                                            shippingPoint = "UNK";
                                            shippingPointDescription = "UNK";

                                            break;
                                        }
                                }






                                productDescription = item.SupplierProductDescription.Replace("'", "") + " " + item.ProductFormCategory;
                                productDescription.Replace("&", " ");
                                lineComment = item.LineComment.Replace("'", "");
                                lineComment = item.LineComment.Replace("&", " and ");

                                XMLstring += "<Item ";
                                XMLstring += " BrokerLineItemNumber='" + item.ID + "'";
                                XMLstring += " BrokerMaterialNumber='" + "" + "'";
                                XMLstring += " VendorMaterialNumber='" + item.ProductID + "'";
                                XMLstring += " MaterialDescription='" + productDescription + "'";
                                XMLstring += " OrderdedQty='" + item.OrderQty + "'";
                                XMLstring += " ConfirmedQty='" + item.OrderQty + "'";
                                XMLstring += " UnitOfMeasure='" + "EACH" + "'";
                                XMLstring += " ScheduledShipDate='" + growerOrder.ShipDate.ToString("yyyy-MM-dd") + "'";
                                XMLstring += " LineItemComment='" + item.LineComment + "'";
                                XMLstring += " IncomingType='" + "N" + "'";
                                XMLstring += " TagRatio='" + tagRatio + "'";
                                XMLstring += " TagRatioDescription='" + "" + "'";
                                XMLstring += " PlantCode='" + plantCode + "'";
                                XMLstring += " PlantCodeDescription='" + plantCodeDescription + "'";
                                XMLstring += " ShippingPoint='" + shippingPoint + "'";
                                XMLstring += " ShippingPointDescription='" + shippingPointDescription + "'";
                                XMLstring += " MaterialGroup='" + "" + "'";
                                XMLstring += " />";


                            }
                            XMLstring += "</OrderDetails>";

                            XMLstring += "</McHutchisonPurchaseOrder> ";

                            break;
                        }
                    case "Vaughans":
                        {
                            var tagRatio = growerOrder.TagRatio;
                            if (tagRatio == "NoTag")
                            {
                                tagRatio = "0";
                            }
                            if (tagRatio == "TagInc")
                            {
                                tagRatio = "1";
                            }
                            var growerShipMethod = "";
                            var plantCode = "";
                            var plantCodeDescription = "";
                            var shippingPoint = "";
                            var shippingPointDescription = "";
                            if (growerOrder.GrowerShipMethodCode == "")
                            {
                                growerShipMethod = "FEDEX";
                            }
                            else
                            {
                                growerShipMethod = growerOrder.GrowerShipMethodCode;
                            }
                            var currentDate = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");//2016-03-21T12:11:05

                            XMLstring = "<VaughansPurchaseOrder> ";
                            XMLstring += "<Identification Reference_ID='" + growerOrder.OrderGuid + "' Date='" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss") + "' MessageProtocol_ID='VaughansPurchaseOrder' FromOrg_ID='GFM' ToOrg_ID='NAV' SenderOrg_ID='NAV'";
                            XMLstring += " />";

                            XMLstring += "<OrderHeader";
                            XMLstring += " VendorOrderID='" + growerOrder.OrderNo + "'";
                            XMLstring += " BrokerOrderID='" + growerOrder.BrokerOrderNo + "'";
                            XMLstring += " CustomerPONumber='" + growerOrder.CustomerPoNo + "'";
                            XMLstring += " BrokerCreatedDate='" + growerOrder.OrderTime.ToString("yyyy-MM-dd") + "'";
                            XMLstring += " BrokerCreatedTime='" + growerOrder.OrderTime.ToString("Thh:mm:ss") + "'";
                            XMLstring += " RequestedShipDate='" + growerOrder.ShipDate.ToString("yyyy-MM-dd") + "'";
                            XMLstring += " ShipMethod='" + growerShipMethod + "'";
                            XMLstring += " ShipMethodDesc='" + "" + "'";
                            XMLstring += " SubstitutionOK='" + "1" + "'";
                            XMLstring += " VendorNumber='" + "" + "'";
                            XMLstring += " HeaderComments='" + growerOrder.OrderDescription + "'";
                            XMLstring += " IncomingType='" + "N" + "'";
                            XMLstring += " SalesRepName='" + "" + "'";
                            XMLstring += " TagRatio='" + tagRatio + "'";
                            XMLstring += " TagRatioDescription='" + "" + "'";
                            XMLstring += " >";

                            XMLstring += "<SoldTo";
                            XMLstring += " BrokerSoldToNumber='" + "" + "'";
                            XMLstring += " SoldToName='" + growerOrder.GrowerName + "'";
                            XMLstring += " SoldToContactName='" + "" + "'";
                            XMLstring += " SoldToAddress1='" + growerOrder.ShipToAddress1 + "'";
                            XMLstring += " SoldToCity='" + growerOrder.ShipToCity + "'";
                            XMLstring += " SoldToState='" + growerOrder.ShipToStateCode + "'";
                            XMLstring += " SoldToCountry='" + growerOrder.ShipToCountryCode + "'";
                            XMLstring += " SoldToZip='" + growerOrder.ShipToZipCode + "'";
                            XMLstring += " SoldToFax='" + "" + "'";
                            XMLstring += " SoldToPhone='" + growerOrder.ShipToPhone + "'";
                            XMLstring += " />";

                            XMLstring += "<ShipTo";
                            XMLstring += " BrokerShipToNumber='" + "" + "'";
                            XMLstring += " ShipToName='" + growerOrder.GrowerName + "'";
                            XMLstring += " ShipToContactName='" + "" + "'";
                            XMLstring += " ShipToAddress1='" + growerOrder.ShipToAddress1 + "'";
                            XMLstring += " ShipToCity='" + growerOrder.ShipToCity + "'";
                            XMLstring += " ShipToState='" + growerOrder.ShipToStateCode + "'";
                            XMLstring += " ShipToCountry='" + growerOrder.ShipToCountryCode + "'";
                            XMLstring += " ShipToZip='" + growerOrder.ShipToZipCode + "'";
                            XMLstring += " ShipToFax='" + "" + "'";
                            XMLstring += " ShipToPhone='" + growerOrder.ShipToPhone + "'";
                            XMLstring += " />";

                            XMLstring += "</OrderHeader>";

                            XMLstring += "<OrderDetails>";
                            foreach (var item in orderLines)
                            {

                                switch (item.SupplierCode)
                                {
                                    case "VMX":

                                        {
                                            plantCode = "GRFV";
                                            plantCodeDescription = "Greenfuse Botanicals - Vivero";
                                            shippingPoint = "GRFV";
                                            shippingPointDescription = "Greenfuse Botanicals - Vivero";
                                            break;
                                        }
                                    case "INN":

                                        {
                                            plantCode = "GRFI";
                                            plantCodeDescription = "Greenfuse Botanicals - Innova";
                                            shippingPoint = "GRFI";
                                            shippingPointDescription = "Greenfuse Botanicals - Innova";
                                            break;
                                        }
                                    case "STP":

                                        {
                                            plantCode = "GRFS";
                                            plantCodeDescription = "Greenfuse Botanicals - Santa Paula";
                                            shippingPoint = "GRFS";
                                            shippingPointDescription = "Greenfuse Botanicals - Santa Paula";
                                            break;
                                        }
                                    default:
                                        {
                                            plantCode = "UNK";
                                            plantCodeDescription = "UNK";
                                            shippingPoint = "UNK";
                                            shippingPointDescription = "UNK";

                                            break;
                                        }
                                }






                                productDescription = item.SupplierProductDescription.Replace("'", "") + " " + item.ProductFormCategory;
                                productDescription.Replace("&", " ");
                                lineComment = item.LineComment.Replace("'", "");
                                lineComment = item.LineComment.Replace("&", " and ");

                                XMLstring += "<Item ";
                                XMLstring += " BrokerLineItemNumber='" + item.ID + "'";
                                XMLstring += " BrokerMaterialNumber='" + "" + "'";
                                XMLstring += " VendorMaterialNumber='" + item.ProductID + "'";
                                XMLstring += " MaterialDescription='" + productDescription + "'";
                                XMLstring += " OrderdedQty='" + item.OrderQty + "'";
                                XMLstring += " ConfirmedQty='" + item.OrderQty + "'";
                                XMLstring += " UnitOfMeasure='" + "EACH" + "'";
                                XMLstring += " ScheduledShipDate='" + growerOrder.ShipDate.ToString("yyyy-MM-dd") + "'";
                                XMLstring += " LineItemComment='" + item.LineComment + "'";
                                XMLstring += " IncomingType='" + "N" + "'";
                                XMLstring += " TagRatio='" + tagRatio + "'";
                                XMLstring += " TagRatioDescription='" + "" + "'";
                                XMLstring += " PlantCode='" + plantCode + "'";
                                XMLstring += " PlantCodeDescription='" + plantCodeDescription + "'";
                                XMLstring += " ShippingPoint='" + shippingPoint + "'";
                                XMLstring += " ShippingPointDescription='" + shippingPointDescription + "'";
                                XMLstring += " MaterialGroup='" + "" + "'";
                                XMLstring += " />";


                            }
                            XMLstring += "</OrderDetails>";

                            XMLstring += "</VaughansPurchaseOrder> ";

                            break;
                        }
                    default:
                        {
                            break;
                        }

                }
            }
            catch (Exception ex)
            {
                var oDB = new DBStuff();
                var bRet = oDB.AddErrorLog("B2B Order Sender Error", "Error in GenerateXMLString: " + ex.Message, "To Broker", brokerName, "");

                if (bRet == false)
                {
                    MessageBox.Show("email failed to send");
                }
            }
            return XMLstring;
        }

        private void ckAvailToMcHutch_CheckedChanged(object sender, EventArgs e)
        {



        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
