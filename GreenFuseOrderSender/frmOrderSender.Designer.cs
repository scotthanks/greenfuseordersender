﻿namespace GreenFuseOrderSender
{
    partial class frmOrderSender
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgSending = new System.Windows.Forms.DataGridView();
            this.dsSending_B2BStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusMessage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsSending_GrowerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrowerEmail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsSending_B2BOrderURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderGuid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GrowerGuid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrokerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrokerGuid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrokerB2BOrderURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B2BUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B2BPassword = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmdSend = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ckGetSBIOrders = new System.Windows.Forms.CheckBox();
<<<<<<< HEAD:GreenFuseOrderSender/frmOrderSender.Designer.cs
            this.ckAvailToMcHutch = new System.Windows.Forms.CheckBox();
            this.ckSendBrokerOrders = new System.Windows.Forms.CheckBox();
            this.ckGetMchutch = new System.Windows.Forms.CheckBox();
=======
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
>>>>>>> ab0a5e5326619d03f9fd3756344a7640de4211e6:EPSOrderSender/frmOrderSender.Designer.cs
            ((System.ComponentModel.ISupportInitialize)(this.dgSending)).BeginInit();
            this.SuspendLayout();
            // 
            // dgSending
            // 
            this.dgSending.AllowUserToAddRows = false;
            this.dgSending.AllowUserToDeleteRows = false;
            this.dgSending.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSending.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dsSending_B2BStatus,
            this.StatusMessage,
            this.dsSending_GrowerName,
            this.GrowerEmail,
            this.dsSending_B2BOrderURL,
            this.OrderGuid,
            this.GrowerGuid,
            this.BrokerName,
            this.BrokerGuid,
            this.BrokerB2BOrderURL,
            this.B2BUserName,
            this.B2BPassword});
            this.dgSending.Location = new System.Drawing.Point(32, 25);
            this.dgSending.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgSending.Name = "dgSending";
            this.dgSending.ReadOnly = true;
            this.dgSending.Size = new System.Drawing.Size(1195, 659);
            this.dgSending.TabIndex = 7;
<<<<<<< HEAD:GreenFuseOrderSender/frmOrderSender.Designer.cs
=======
            this.dgSending.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSending_CellContentClick);
>>>>>>> ab0a5e5326619d03f9fd3756344a7640de4211e6:EPSOrderSender/frmOrderSender.Designer.cs
            // 
            // dsSending_B2BStatus
            // 
            this.dsSending_B2BStatus.HeaderText = "B2BStatus";
            this.dsSending_B2BStatus.Name = "dsSending_B2BStatus";
            this.dsSending_B2BStatus.ReadOnly = true;
            // 
            // StatusMessage
            // 
            this.StatusMessage.HeaderText = "Status Message";
            this.StatusMessage.Name = "StatusMessage";
            this.StatusMessage.ReadOnly = true;
            // 
            // dsSending_GrowerName
            // 
            this.dsSending_GrowerName.HeaderText = "Grower Name";
            this.dsSending_GrowerName.Name = "dsSending_GrowerName";
            this.dsSending_GrowerName.ReadOnly = true;
            // 
            // GrowerEmail
            // 
            this.GrowerEmail.HeaderText = "Grower Email";
            this.GrowerEmail.Name = "GrowerEmail";
            this.GrowerEmail.ReadOnly = true;
            // 
            // dsSending_B2BOrderURL
            // 
            this.dsSending_B2BOrderURL.HeaderText = "Grower B2B Order URL";
            this.dsSending_B2BOrderURL.Name = "dsSending_B2BOrderURL";
            this.dsSending_B2BOrderURL.ReadOnly = true;
            // 
            // OrderGuid
            // 
            this.OrderGuid.HeaderText = "Order Guid";
            this.OrderGuid.Name = "OrderGuid";
            this.OrderGuid.ReadOnly = true;
            // 
            // GrowerGuid
            // 
            this.GrowerGuid.HeaderText = "Grower Guid";
            this.GrowerGuid.Name = "GrowerGuid";
            this.GrowerGuid.ReadOnly = true;
            // 
            // BrokerName
            // 
            this.BrokerName.HeaderText = "BrokerName";
            this.BrokerName.Name = "BrokerName";
            this.BrokerName.ReadOnly = true;
            // 
            // BrokerGuid
            // 
            this.BrokerGuid.HeaderText = "BrokerGuid";
            this.BrokerGuid.Name = "BrokerGuid";
            this.BrokerGuid.ReadOnly = true;
            // 
            // BrokerB2BOrderURL
            // 
            this.BrokerB2BOrderURL.HeaderText = "BrokerB2BOrderURL";
            this.BrokerB2BOrderURL.Name = "BrokerB2BOrderURL";
            this.BrokerB2BOrderURL.ReadOnly = true;
            // 
            // B2BUserName
            // 
            this.B2BUserName.HeaderText = "B2BUserName";
            this.B2BUserName.Name = "B2BUserName";
            this.B2BUserName.ReadOnly = true;
            // 
            // B2BPassword
            // 
            this.B2BPassword.HeaderText = "B2BPassword";
            this.B2BPassword.Name = "B2BPassword";
            this.B2BPassword.ReadOnly = true;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cmdSend
            // 
            this.cmdSend.Location = new System.Drawing.Point(32, 708);
            this.cmdSend.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Size = new System.Drawing.Size(297, 80);
            this.cmdSend.TabIndex = 8;
<<<<<<< HEAD:GreenFuseOrderSender/frmOrderSender.Designer.cs
            this.cmdSend.Text = "Send Now and  refresh Orders";
=======
            this.cmdSend.Text = "Send Now and Get SBI Orders";
>>>>>>> ab0a5e5326619d03f9fd3756344a7640de4211e6:EPSOrderSender/frmOrderSender.Designer.cs
            this.cmdSend.UseVisualStyleBackColor = true;
            this.cmdSend.Click += new System.EventHandler(this.cmdSend_Click);
            // 
            // label1
            // 
<<<<<<< HEAD:GreenFuseOrderSender/frmOrderSender.Designer.cs
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 1;
            // 
            // ckGetSBIOrders
            // 
            this.ckGetSBIOrders.Location = new System.Drawing.Point(0, 0);
            this.ckGetSBIOrders.Name = "ckGetSBIOrders";
            this.ckGetSBIOrders.Size = new System.Drawing.Size(104, 24);
            this.ckGetSBIOrders.TabIndex = 0;
            // 
            // ckAvailToMcHutch
            // 
            this.ckAvailToMcHutch.AutoSize = true;
            this.ckAvailToMcHutch.Location = new System.Drawing.Point(972, 708);
            this.ckAvailToMcHutch.Name = "ckAvailToMcHutch";
            this.ckAvailToMcHutch.Size = new System.Drawing.Size(264, 24);
            this.ckAvailToMcHutch.TabIndex = 9;
            this.ckAvailToMcHutch.Text = "Send Avail to McHuch (WinSCP)";
            this.ckAvailToMcHutch.UseVisualStyleBackColor = true;
            this.ckAvailToMcHutch.CheckedChanged += new System.EventHandler(this.ckAvailToMcHutch_CheckedChanged);
            // 
            // ckSendBrokerOrders
            // 
            this.ckSendBrokerOrders.AutoSize = true;
            this.ckSendBrokerOrders.Location = new System.Drawing.Point(712, 708);
            this.ckSendBrokerOrders.Name = "ckSendBrokerOrders";
            this.ckSendBrokerOrders.Size = new System.Drawing.Size(176, 24);
            this.ckSendBrokerOrders.TabIndex = 10;
            this.ckSendBrokerOrders.Text = "Send Broker Orders";
            this.ckSendBrokerOrders.UseVisualStyleBackColor = true;
            // 
            // ckGetMchutch
            // 
            this.ckGetMchutch.AutoSize = true;
            this.ckGetMchutch.Location = new System.Drawing.Point(972, 753);
            this.ckGetMchutch.Name = "ckGetMchutch";
            this.ckGetMchutch.Size = new System.Drawing.Size(291, 24);
            this.ckGetMchutch.TabIndex = 11;
            this.ckGetMchutch.Text = "Get Orders From McHuch (WinSCP)";
            this.ckGetMchutch.UseVisualStyleBackColor = true;
            this.ckGetMchutch.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
=======
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(338, 708);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(298, 32);
            this.label1.TabIndex = 9;
            this.label1.Text = "Not yet doing anything";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // ckGetSBIOrders
            // 
            this.ckGetSBIOrders.AutoSize = true;
            this.ckGetSBIOrders.Location = new System.Drawing.Point(722, 708);
            this.ckGetSBIOrders.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ckGetSBIOrders.Name = "ckGetSBIOrders";
            this.ckGetSBIOrders.Size = new System.Drawing.Size(145, 24);
            this.ckGetSBIOrders.TabIndex = 10;
            this.ckGetSBIOrders.Text = "Get SBI Orders";
            this.ckGetSBIOrders.UseVisualStyleBackColor = true;
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timer3
            // 
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
>>>>>>> ab0a5e5326619d03f9fd3756344a7640de4211e6:EPSOrderSender/frmOrderSender.Designer.cs
            // 
            // frmOrderSender
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1257, 869);
            this.Controls.Add(this.ckGetMchutch);
            this.Controls.Add(this.ckSendBrokerOrders);
            this.Controls.Add(this.ckAvailToMcHutch);
            this.Controls.Add(this.ckGetSBIOrders);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdSend);
            this.Controls.Add(this.dgSending);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmOrderSender";
<<<<<<< HEAD:GreenFuseOrderSender/frmOrderSender.Designer.cs
            this.Text = "Green Fuse Order Sender";
=======
            this.Text = "EPS Order Sender";
>>>>>>> ab0a5e5326619d03f9fd3756344a7640de4211e6:EPSOrderSender/frmOrderSender.Designer.cs
            this.Load += new System.EventHandler(this.frmOrderSender_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgSending)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgSending;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button cmdSend;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox ckGetSBIOrders;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsSending_B2BStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusMessage;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsSending_GrowerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn GrowerEmail;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsSending_B2BOrderURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderGuid;
        private System.Windows.Forms.DataGridViewTextBoxColumn GrowerGuid;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrokerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrokerGuid;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrokerB2BOrderURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn B2BUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn B2BPassword;
        private System.Windows.Forms.CheckBox ckAvailToMcHutch;
        private System.Windows.Forms.CheckBox ckSendBrokerOrders;
        private System.Windows.Forms.CheckBox ckGetMchutch;
    }
}

