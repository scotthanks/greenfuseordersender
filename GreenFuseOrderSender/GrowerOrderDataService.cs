﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
namespace GreenFuseOrderSender
{
    class GrowerOrderDataService
    {
        public GrowerOrderDataService()       
        {

        }
        
        public GrowerOrderB2B GetB2BOrder(Guid orderGuid,out string errorMessage)
        {
            var item = new GrowerOrderB2B();

            try
            {
                errorMessage = "";
                DBStuff oDBStuff = new DBStuff();
                oDBStuff.sEnvironment = "Prod";
                oDBStuff.sConnectionType = "Data";
                string sConn = oDBStuff.GetConnectionString(oDBStuff.sConnectionType, oDBStuff.sEnvironment);

                SqlConnection connection = new SqlConnection(sConn);
               
              
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[GrowerOrderB2BGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                           .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                           .Value = orderGuid;

                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {

                            item.OrderGuid = (Guid)reader["OrderGuid"];
                            item.GrowerName = (string)reader["GrowerName"];
                            item.CustomerPoNo = (string)reader["CustomerPoNo"];
                            item.OrderDescription = (string)reader["OrderDescription"];
                            item.OrderNo = (string)reader["OrderNo"];
                            item.SellerOrderNo = (string)reader["SellerOrderNo"];
                            item.ProductFormCategoryCode = (string)reader["ProductFormCategoryCode"];
                            item.ShipWeekString = (string)reader["ShipWeekString"];
                            item.OrderDate = (DateTime)reader["OrderDate"];
                            item.OrderTime = (DateTime)reader["OrderTime"];
                            item.ShipDate = (DateTime)reader["ShipDate"];
                            item.ShipMethodCode = (string)reader["ShipMethodCode"];
                            item.ShipMethodDescription = (string)reader["ShipMethodName"];
                            item.GrowerShipMethodCode = (string)reader["GrowerShipMethodCode"];
                            item.ShipMethodDescription = (string)reader["ShipMethodName"];
                            item.SubstitutionOK = (string)reader["SubstitutionOK"];
                            item.FreightPaymentMethod = (string)reader["FreightPaymentMethod"];
                            item.SellerNumber = (string)reader["SellerNumber"];
                            item.AirportCode = (string)reader["AirportCode"];
                            item.BackOrderOK = (string)reader["BackOrderOK"];
                            item.HeaderComments = (string)reader["HeaderComments"];
                            item.ShippingInstructions = (string)reader["ShippingInstructions"];
                            item.SalesRepName = (string)reader["SalesRepName"];
                            item.TagRatio = (string)reader["TagRatioCode"];
                            item.TagRatioDescription = (string)reader["TagRatioName"];
                            item.GrowerID = (string)reader["GrowerID"];
                            item.GrowerAddressName = (string)reader["GrowerAddressName"];
                            item.GrowerContact = (string)reader["GrowerContact"];
                            item.GrowerAddress1 = (string)reader["GrowerAddress1"];
                            item.GrowerAddress2 = (string)reader["GrowerAddress2"];
                            item.GrowerCity = (string)reader["GrowerCity"];
                            item.GrowerStateCode = (string)reader["GrowerStateCode"];
                            item.GrowerCountryCode = (string)reader["GrowerCountryCode"];
                            item.GrowerZipCode = (string)reader["GrowerZipCode"];
                            item.GrowerFax = (string)reader["GrowerFax"];
                            item.GrowerPhone = (string)reader["GrowerPhone"];
                            item.GrowerCountryCode2 = (string)reader["GrowerCountryCode2"];
                            item.GrowerAddressShipToID = (string)reader["GrowerAddressShipToID"];
                            item.ShipToID = (string)reader["GrowerAddressShipToID"];
                            item.ShipToName = (string)reader["ShipToName"];
                            item.ShipToContact = (string)reader["ShipToContact"];
                            item.ShipToAddress1 = (string)reader["ShipToAddress1"];
                            item.ShipToAddress2 = (string)reader["ShipToAddress2"];
                            item.ShipToCity = (string)reader["ShipToCity"];
                            item.ShipToStateCode = (string)reader["ShipToStateCode"];
                            item.ShipToCountryCode = (string)reader["ShipToCountryCode"];
                            item.ShipToZipCode = (string)reader["ShipToZipCode"];
                            item.ShipToFax = (string)reader["ShipToFax"];
                            item.ShipToPhone = (string)reader["ShipToPhone"];
                            item.ShipToCountryCode2 = (string)reader["ShipToCountryCode2"];
                           
                            item.B2BCustomerName = (string)reader["B2BCustomerName"];
                            item.SupplierUrlForOrder = (string)reader["B2BUrl"];
                            item.GrowerURLForOrder = (string)reader["GrowerURLForOrder"];
                            item.GrowerEmail = (string)reader["GrowerEmail"];
                            item.RevisionNumber = (long)reader["RevisionNumber"];
                            item.BrokerOrderNo = (string)reader["BrokerOrderNo"];
                        }
                        connection.Close();
                    }
                }
              
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return item;

        }
        public List<OrderLine> GetOrderLines(Guid orderGuid, out string errorMessage)
        {
            var list = new List<OrderLine>();
            errorMessage = "";
            try
            {
                errorMessage = "";
                DBStuff oDBStuff = new DBStuff();
                oDBStuff.sEnvironment = "Prod";
                oDBStuff.sConnectionType = "Data";
                string sConn = oDBStuff.GetConnectionString(oDBStuff.sConnectionType, oDBStuff.sEnvironment);

                SqlConnection connection = new SqlConnection(sConn);
    
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[OrderLinesGet]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                           .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                           .Value = orderGuid;






                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            var item = new OrderLine();
                            item.Guid = (Guid)reader["Guid"];
                            item.ID = (int)reader["ID"];
                            item.ProductID = (long)reader["ProductID"];
                            item.GrowerOrderGuid = (Guid)reader["GrowerOrderGuid"];
                            item.SupplierOrderGuid = (Guid)reader["SupplierOrderGuid"];
                            item.SupplierCode = (string)reader["SupplierCode"];
                            item.SupplierName = (string)reader["SupplierName"];
                            item.SellerCode = (string)reader["SellerCode"];
                            item.ProductGuid = (Guid)reader["ProductGuid"];
                            item.ProductForm = (string)reader["ProductForm"];
                            item.ProductFormCategory = (string)reader["ProductFormCategory"];
                            item.OrderB2BStatus = (string)reader["OrderB2BStatus"];
                            item.SellerPlantCode = (string)reader["SellerPlantCode"];
                            item.SellerPlantCodeDescription = (string)reader["SellerPlantCodeDescription"];
                            item.SupplierProductIdentifier = (string)reader["SupplierProductIdentifier"];
                            item.SupplierProductDescription = (string)reader["SupplierProductDescription"];
                            item.OrderQty = (Int32)reader["OrderQty"];
                            item.LineComment = (string)reader["LineComment"];
                            item.TagRatioCode = (string)reader["TagRatioCode"];
                            item.OrderLineStatus = (string)reader["OrderLineStatus"];
                            item.Price = (decimal)reader["ActualPrice"];
                            list.Add(item);
                        }
                        connection.Close();
                    }
                }
        
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }

            return list;

        }
        public long B2BIntegrationAddXMLData(string b2BType, string communicationDirection, string sellerCode, string supplierCode, string XMLData,
                string responseXMLData, Guid orderGuid, string b2BStatus, string statusMessage, Guid growerGuid, Guid brokerGuid, out string errorMessage)
        {
            long b2BjobNumber = -1;
            try
            {
                errorMessage = "";
                DBStuff oDBStuff = new DBStuff();
                oDBStuff.sEnvironment = "Prod";
                oDBStuff.sConnectionType = "B2B";
                string sConn = oDBStuff.GetConnectionString(oDBStuff.sConnectionType, oDBStuff.sEnvironment);

                SqlConnection connection = new SqlConnection(sConn);
           

                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[B2BIntegrationAddXMLData]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@B2BType", SqlDbType.NVarChar, 20))
                            .Value = b2BType;
                        command.Parameters
                            .Add(new SqlParameter("@CommunicationDirection", SqlDbType.NVarChar, 10))
                            .Value = communicationDirection;
                        command.Parameters
                            .Add(new SqlParameter("@SellerCode", SqlDbType.NVarChar, 10))
                            .Value = sellerCode;
                        command.Parameters
                            .Add(new SqlParameter("@SupplierCode", SqlDbType.NVarChar, 10))
                            .Value = supplierCode;
                        command.Parameters
                            .Add(new SqlParameter("@XMLData", SqlDbType.Xml))
                            .Value = XMLData;
                        command.Parameters
                            .Add(new SqlParameter("@ResponseXMLData", SqlDbType.Xml))
                            .Value = responseXMLData;
                        command.Parameters
                            .Add(new SqlParameter("@OrderGuid", SqlDbType.UniqueIdentifier))
                            .Value = orderGuid;
                        command.Parameters
                           .Add(new SqlParameter("@B2BStatus", SqlDbType.NVarChar, 50))
                           .Value = b2BStatus;
                        command.Parameters
                           .Add(new SqlParameter("@StatusMessage", SqlDbType.NVarChar, 0))
                           .Value = statusMessage;
                        command.Parameters
                           .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                           .Value = growerGuid;
                        command.Parameters
                            .Add(new SqlParameter("@BrokerGuid", SqlDbType.UniqueIdentifier))
                            .Value = brokerGuid;
                        command.Parameters
                              .Add(new SqlParameter("@B2BJobNumber", SqlDbType.BigInt))
                             .Direction = ParameterDirection.Output;
                        connection.Open();

                        var response = (int)command.ExecuteNonQuery();
                        b2BjobNumber = (long)command.Parameters["@B2BJobNumber"].Value;

                        connection.Close();
                    }
                }
               
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            return b2BjobNumber;
        }
        public bool B2BAfterGrowerSendUpdate(Guid orderGuid, out string errorMessage)
        {
           
            var bRet = true;
            try
            {
                errorMessage = "";
                DBStuff oDBStuff = new DBStuff();
                oDBStuff.sEnvironment = "Prod";
                oDBStuff.sConnectionType = "Data";
                string sConn = oDBStuff.GetConnectionString(oDBStuff.sConnectionType, oDBStuff.sEnvironment);

                SqlConnection connection = new SqlConnection(sConn);
               
              
                using (connection)
                {
                    using (var command = new SqlCommand("[dbo].[B2BAfterGrowerSendUpdate]", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                           .Add(new SqlParameter("@GrowerOrderGuid", SqlDbType.UniqueIdentifier))
                           .Value = orderGuid;

                        connection.Open();

                        var iRet = command.ExecuteNonQuery();

 
                        connection.Close();
                    }
                }
              
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                bRet = false;
            }

            return bRet;

        }

        public bool GrowerAvailabilityGetForBroker(Guid growerGuid, string programCodes, int shipweekStart, int shipweekEnd, bool onlyChangesSinceLastSend, Guid brokerGuid, out string availabilityXML)
        {
            var xml = "";

            try
            {

                DBStuff oDBStuff = new DBStuff();
                oDBStuff.sEnvironment = "Prod";
                oDBStuff.sConnectionType = "Data";
                string sConn = oDBStuff.GetConnectionString(oDBStuff.sConnectionType, oDBStuff.sEnvironment);

                SqlConnection connection = new SqlConnection(sConn);

                using (connection)
                {

                    using (var command = new SqlCommand("[dbo].[GrowerAvailabilityGetForBroker]", connection))
                    {
                        command.CommandTimeout = 120;
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters
                            .Add(new SqlParameter("@GrowerGuid", SqlDbType.UniqueIdentifier))
                            .Value = growerGuid;
                        command.Parameters
                            .Add(new SqlParameter("@ProgramCodes", SqlDbType.NVarChar, 100))
                            .Value = programCodes;
                        command.Parameters
                            .Add(new SqlParameter("@StartWeek", SqlDbType.Int))
                            .Value = shipweekStart;
                        command.Parameters
                            .Add(new SqlParameter("@EndWeek", SqlDbType.Int))
                            .Value = shipweekEnd;

                        command.Parameters
                            .Add(new SqlParameter("@OnlyChanges", SqlDbType.Bit))
                            .Value = onlyChangesSinceLastSend;
                        command.Parameters
                            .Add(new SqlParameter("@BrokerGuid", SqlDbType.UniqueIdentifier))
                            .Value = brokerGuid;


                        connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {

                            xml = (string)reader["XMLString"];

                        }
                        connection.Close();
                    }
                }


               

            }
            catch (Exception ex)
            {
                throw ex;
            }





            availabilityXML = xml;
            return true;
        }

    }
}
