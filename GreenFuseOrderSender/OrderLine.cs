﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreenFuseOrderSender
{

    class OrderLine
    {
        public Guid Guid { get; set; }
        public long ID { get; set; }
        public Guid SupplierOrderGuid { get; set; }
        public Guid GrowerOrderGuid { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string SellerCode { get; set; }
        public Guid ProductGuid { get; set; }
        public long ProductID { get; set; }
        public string ProductForm { get; set; }
        public string ProductFormCategory { get; set; }
        public string SupplierProductIdentifier { get; set; }
        public string SupplierProductDescription { get; set; }
        public Int32 OrderQty { get; set; }
        public string OrderB2BStatus { get; set; }
        public string SellerPlantCode { get; set; }
        public string SellerPlantCodeDescription { get; set; }
        public string LineComment { get; set; }
        public string TagRatioCode { get; set; }
        public string OrderLineStatus { get; set; }
        public decimal Price { get; set; }
    }
}
