﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace GreenFuseOrderSender
{
    public class DBStuff
    {

        public string sEnvironment;
        public string sConnectionType;

        public String MyStripDBField(string sInput)
        {
            string sResult = sInput.Replace("'", "''"); ;
            return sResult;
        }

        public int ExecuteSQL(string theSql)
        {
            string sConn = GetConnectionString(sConnectionType, sEnvironment);

            SqlConnection conn = new SqlConnection(sConn);
            conn.Open();

            SqlCommand oCommandUpdateNA1 = new SqlCommand(theSql, conn);
            int iRet = oCommandUpdateNA1.ExecuteNonQuery();
            conn.Close();
            return iRet;
        }

        public bool B2BLogMessage(string logMessage)
        {
            logMessage = logMessage.Replace("'", "_");
            try
            {

                DBStuff oDBStuff = new DBStuff();
                oDBStuff.sEnvironment = "Prod";
                oDBStuff.sConnectionType = "B2B";
                var sSQL = "exec [dbo].[B2BLogAdd] '" + logMessage + "'";
                var bret = oDBStuff.ExecuteSQL(sSQL);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return true;
        }
        public DataTable GetDataFromSQL(string theSql)
        {

            try
            {
                string sConn = GetConnectionString(sConnectionType, sEnvironment);

                SqlConnection conn = new SqlConnection(sConn);
                conn.Open();

                SqlCommand oCommand = new SqlCommand(theSql, conn);
                SqlDataReader oCommandReader = oCommand.ExecuteReader();
                DataTable oDataTable = new DataTable();
                oDataTable.Load(oCommandReader);
                conn.Close();
                return oDataTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }





        public string GetConnectionString(string sType, string sEnvironment)
        {

            string sConn = "";
            switch (sType)
            {
                
                case "B2B":
                    {

                        sConn = "Data Source=";
                        sConn += GetConfig("ProdDataServer");
                        sConn += ";database=epsB2BProd; uid=epsdbadmin;pwd=awsM0nitor13";



                        break;
                    }
                case "Security":
                    {
                        if (sEnvironment == "Prod")
                        {
                            sConn = "Data Source=";
                            sConn += GetConfig("ProdDataServer");
                            sConn += ";database=epsMembershipProd; uid=epsdbadmin;pwd=awsM0nitor13";
                        }
                        else
                        {
                            if (sEnvironment == "Stage")
                            {
                                sConn = "Data Source=";
                                sConn += GetConfig("StageDataServer");
                                sConn += ";database=epsMembershipProd; uid=epsdbadmin;pwd=awsM0nitor13";
                            }
                            else
                            {
                                //   sConn = "Data Source=epsdev.cal4bpp33ynn.us-west-1.rds.amazonaws.com,1433;database=epsMembershipDev; uid=epsdbadmin;pwd=awsM0nitor13";
                                //DEV now on PROD server
                                sConn = "Data Source=epsprod02.cal4bpp33ynn.us-west-1.rds.amazonaws.com,1433;database=epsMembershipDev; uid=epsdbadmin;pwd=awsM0nitor13";



                            }
                        }


                        break;
                    }
                default:
                    {
                        if (sEnvironment == "Prod")
                        {
                            sConn = "Data Source=";
                            sConn += GetConfig("ProdDataServer");
                            sConn += ";database=epsDataProd; uid=epsdbadmin;pwd=awsM0nitor13";
                        }
                        else
                        {
                            if (sEnvironment == "Stage")
                            {
                                sConn = "Data Source=";
                                sConn += GetConfig("StageDataServer");
                                sConn += ";database=epsDataProd; uid=epsdbadmin;pwd=awsM0nitor13";
                            }
                            else
                            {
                                //        sConn = "Data Source=epsdev.cal4bpp33ynn.us-west-1.rds.amazonaws.com,1433;database=epsDataDev; uid=epsdbadmin;pwd=awsM0nitor13";
                                // DEV now on PROD server
                                sConn = "Data Source=greenfuseprod.cal4bpp33ynn.us-west-1.rds.amazonaws.com,1433;database=epsDataDev; uid=epsdbadmin;pwd=awsM0nitor13";
                                
                            }
                        }
                        break;       // break necessary on default
                    }
            }
            return sConn;
        }


        public string GetConfig(string sCode)
        {
            DBStuff oDBStuff = new DBStuff();
            oDBStuff.sEnvironment = "Dev";
            oDBStuff.sConnectionType = "Data";


            //Auth Users
            string sSql = "SELECT Value from Config where Code = '" + sCode + "'";

            DataTable thedataTable = oDBStuff.GetDataFromSQL(sSql);
            try
            {
                return thedataTable.Rows[0]["Value"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }
        public bool AddErrorLog(string subject, string body, string direction, string growerName, string orderNo)
        {
            var bRet = false;
            string sConn = GetConnectionString("Data", "Prod");
            SqlConnection conn = new SqlConnection(sConn);
            conn.Open();
            try
            {

                using (conn)
                {

                    using (var command = new SqlCommand("[dbo].[ErrorLogAdd]", conn))
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@Subject", SqlDbType.NVarChar, 200))
                            .Value = subject;
                        command.Parameters
                            .Add(new SqlParameter("@Body", SqlDbType.NVarChar, 0))
                            .Value = body;
                        command.Parameters
                            .Add(new SqlParameter("@Direction", SqlDbType.NVarChar, 100))
                            .Value = direction;
                        command.Parameters
                            .Add(new SqlParameter("@GrowerName", SqlDbType.NVarChar, 200))
                            .Value = growerName;
                        command.Parameters
                            .Add(new SqlParameter("@OrderNo", SqlDbType.NVarChar, 100))
                            .Value = orderNo;


                        var iReturn = command.ExecuteNonQuery();
                        conn.Close();
                    }
                }
                bRet = true;

            }
            catch (Exception ex)
            {
                bRet = false;
            }
            finally
            {
                conn.Close();

            }

            return bRet;
        }
        public bool AddEmail(string subject, string body, int delaySendMinutes, string emailType, string emailFrom, string emailTo, string emailCC, string emailBCC, Guid orderGuid, int updateCount, int addCount, int cancelCount)
        {
            var bRet = false;
            string sConn = GetConnectionString("Data", "Prod");

            SqlConnection conn = new SqlConnection(sConn);
            conn.Open();
            try
            {
                using (conn)
                {

                    using (var command = new SqlCommand("[dbo].[EmailQueueAdd]", conn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters
                            .Add(new SqlParameter("@sSubject", SqlDbType.NVarChar, 200))
                            .Value = subject;
                        command.Parameters
                            .Add(new SqlParameter("@sBody", SqlDbType.NVarChar, 0))
                            .Value = body;
                        command.Parameters
                            .Add(new SqlParameter("@iDelaySendMinutes", SqlDbType.Int))
                            .Value = delaySendMinutes;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailType", SqlDbType.NVarChar, 50))
                            .Value = emailType;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailTo", SqlDbType.NVarChar, 100))
                            .Value = emailTo;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailCC", SqlDbType.NVarChar, 100))
                            .Value = emailCC;
                        command.Parameters
                            .Add(new SqlParameter("@sEmailBCC", SqlDbType.NVarChar, 100))
                            .Value = emailBCC;
                        command.Parameters
                            .Add(new SqlParameter("@gOrderGuid", SqlDbType.UniqueIdentifier))
                            .Value = orderGuid;
                        command.Parameters
                            .Add(new SqlParameter("@iUpdateCount", SqlDbType.Int))
                            .Value = updateCount;
                        command.Parameters
                            .Add(new SqlParameter("@iAddCount", SqlDbType.Int))
                            .Value = addCount;
                        command.Parameters
                            .Add(new SqlParameter("@iCancelCount", SqlDbType.Int))
                            .Value = cancelCount;
                        command.Parameters
                           .Add(new SqlParameter("@sEmailFrom", SqlDbType.NVarChar, 100))
                           .Value = emailFrom;

                        var iReturn = command.ExecuteNonQuery();

                    }
                }


                bRet = true;






            }
            catch (Exception ex)
            {
                bRet = false;
            }
            finally
            {
                conn.Close();

            }

            return bRet;




        }
    }
}
